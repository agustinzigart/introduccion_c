#include "entitys.c"

//Primera version realizada

// Person setName(Person person, char name[100]){
//     for(int i = 0; i < sizeof(name); i++){
//         person.name[i] = name[i];
//     }
//     return person;
// }


/*
  Encontre que existe la funcion strcpy que copia los caracteres del segundo
  parametro a la direccion de memoria del primero. Deja de hacerlo cuando
  encuentra el caracter nulo.
*/

Person setName(Person person, char name[100]){
    strcpy(person.name,name);
    return person;
}

Person setAge(Person person, int age){
    person.age = age;
    return person;
}