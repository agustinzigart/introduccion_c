#include <stdio.h>
#include "functions.c"
int main()
{
    LinkedList list = createLinkedList();
    LinkedList orderList = createLinkedList();

    insert(&orderList, 2);
    insert(&orderList, 3);
    insert(&orderList, 1);

    append(&list, 5);
    append(&list, 15);
    append(&list, 3);
    preppend(&list, 10);

    printf("LONGITUD: %d\n", length(list));

    printf("VALOR: %d\n", getElement(list, 0));

    printf("LISTA COMPLETA: \n");
    printList(&list);

    printf("LISTA SIN EL 10: \n");
    removeElement(&list, 10);
    printList(&list);

    printf("LISTA ORDENADA COMPLETA: \n");
    printList(&orderList);

    printf("LISTA ORDENADA SIN EL 3: \n");
    removeElement(&orderList, 3);
    printList(&orderList);

    return 0;
}
