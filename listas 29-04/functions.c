#include "entitys.c"
#include <malloc.h>

LinkedList createLinkedList()
{
    LinkedList list;
    list.head = NULL;
    list.length = 0;
    return list;
}

void append(LinkedList *list, int element)
{
    Node *newNode = (Node *)malloc(sizeof(Node));
    newNode->value = element;
    newNode->next = NULL;

    if (list->head == NULL)
    {
        list->head = newNode;
        list->tail = newNode;
    }
    else
    {
        list->tail->next = newNode;
        list->tail = newNode;
    }

    list->length++;
}

void insert(LinkedList *list, int element)
{
    Node *newNode = (Node *)malloc(sizeof(Node));
    newNode->value = element;
    newNode->next = NULL;

    if (list->head == NULL)
    {
        list->head = newNode;
        list->tail = newNode;
        list->length++;
        return;
    }
    else if (list->tail->value < element)
    {
        list->tail->next = newNode;
        list->tail = newNode;
        list->length++;
        return;
    }
    else if (element < list->head->value)
    {
        newNode->next = list->head;
        list->head = newNode;
        list->length++;
        return;
    }

    Node *current = list->head;

    while (current->next != NULL && current->next->value < element)
    {
        current = current->next;
    }

    newNode->next = current->next;
    list->length++;
}

void removeElement(LinkedList *list, int element)
{

    if (list->head == NULL)
    {
        return;
    }

    if (list->head->value == element)
    {
        list->head = list->head->next;
        return;
    }

    Node *current = list->head;

    while (current->next != NULL)
    {
        if (current->next->value == element)
        {
            current->next = current->next->next;
            list->length--;
            return;
        }
        current = current->next;
    }
}

void preppend(LinkedList *list, int element)
{
    Node *newNode = (Node *)malloc(sizeof(Node));
    newNode->value = element;
    newNode->next = NULL;

    if (list->head == NULL)
    {
        list->head = newNode;
        list->tail = newNode;
    }
    else
    {
        newNode->next = list->head;
        list->head = newNode;
    }

    list->length++;
}

int length(LinkedList list)
{
    return list.length;
}

int getElement(LinkedList list, int index)
{
    if (index >= 0 && index < list.length)
    {
        Node *current = list.head;
        for (int i = 0; i < index; i++)
        {
            current = current->next;
        }
        return current->value;
    }
    else
    {
        return -1;
    }
}

void printList(LinkedList *list)
{
    printf("Lista: ");
    Node *current = list->head;
    while (current != NULL)
    {
        printf("%d ", current->value);
        current = current->next;
    }
    printf("\n");
}