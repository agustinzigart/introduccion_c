typedef struct NewNode
{
    int value;
    struct NewNode *next;
} Node;

typedef struct
{
    Node *head;
    Node *tail;
    int length;
} LinkedList;