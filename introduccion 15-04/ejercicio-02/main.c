#include <stdio.h>

int main()
{
    int listOfNumbers[] = {10, 2, 15, 1};
    int max = listOfNumbers[0];
    for (int i = 0; i < sizeof(listOfNumbers) / 4; i++)
    {
        if (max < listOfNumbers[i])
        {
            max = listOfNumbers[i];
        }
    }
    printf("%i", max);
    return max;
}