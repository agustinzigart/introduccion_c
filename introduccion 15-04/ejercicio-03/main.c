#include <stdio.h>

int main()
{
    int listOfNumbers[] = {10, 2, 15, 1};
    int min = listOfNumbers[0];
    for (int i = 0; i < sizeof(listOfNumbers) / 4; i++)
    {
        if (min > listOfNumbers[i])
        {
            min = listOfNumbers[i];
        }
    }
    printf("%i", min);
    return min;
}