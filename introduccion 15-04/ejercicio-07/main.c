#include <stdio.h>

int main()
{
    int valueSelected;

    do
    {
        printf("Seleccione una opcion: \n");
        printf("1. Opcion 1 \n");
        printf("2. Opcion 2 \n");
        printf("3. Opcion 3 \n");
        printf("4. Salir \n");
        scanf("%i", &valueSelected);
        switch (valueSelected)
        {
        case 1:
            printf("Ingreso al primer menu \n");
            break;
        case 2:
            printf("Ingreso al segundo menu \n");
            break;
        case 3:
            printf("Ingreso al tercer menu \n");
            break;
        case 4:
            printf("Salio del menu \n");
            break;

        default:
            printf("Este valor no esta disponible en el menu \n");
            break;
        }

    } while (valueSelected != 4);

    return 0;
}