#include <stdio.h>

float main()
{
    int numbers[3];

    scanf("%i %i %i", &numbers[0], &numbers[1], &numbers[2]);

    int sum = 0;
    for(int i = 0; i < sizeof(numbers) / 4; i++){
        sum += numbers[i];
    }

    float average = sum / (sizeof(numbers) / 4);

    printf("promedio: %f", average);

    return average;
}